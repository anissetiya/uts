package com.example.uts_laundry

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_signin.*

class SignInActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        btnLogoff.setOnClickListener{
            Toast.makeText(this, "Berhasil Log Out", Toast.LENGTH_SHORT).show()
            var intent = Intent(this, MainActivity::class.java)
            startActivity(intent)

        }
        btnMap.setOnClickListener{
            var intent = Intent(this, activitymapbox::class.java)
            startActivity(intent)

        }

        btnLaundry.setOnClickListener{
            var intent = Intent(this, LaundryActivity::class.java)
            startActivity(intent)

        }
        btnAbout.setOnClickListener{
            var intent = Intent(this, AboutActivity::class.java)
            startActivity(intent)

        }
    }
}